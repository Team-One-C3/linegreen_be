from django.contrib import admin
from django.db import router
from django.urls import path
from authApp import views
from authApp.views.ElementosDetalle import ElementosDetalle
from authApp.views.SolRecoleccionView import SolRecoleccionView
from rest_framework_simplejwt.views import(TokenObtainPairView, TokenRefreshView)
urlpatterns = [
    path('admin/', admin.site.urls),
    
    path('crearusuario/',views.UserCreateView.as_view()),
    path('crearsolicitud/',views.SolRecoleccionView.as_view()),
    path('creardetalle/',views.DetalleCreateView.as_view()),
    path('creartip/',views.TipCreateView.as_view()),
    path('consultarelementos/<int:usuario>/<int:elemento>/',views.ElementosDetalle.as_view()),
    path('login/',TokenObtainPairView.as_view()),
    path('consultausuario/<int:pk>',views.UserDetailView.as_view()),
    path('refresh/',TokenRefreshView.as_view()),
    path('actualizaelemento/<int:usuario>/<int:pk>/',views.ElementoActualizar.as_view()),
    path('eliminaelemento/<int:usuario>/<int:pk>/',views.ElementoEliminar.as_view()),
    path('solporusuario/<int:usuario>/',views.ConsultarPorUsuario.as_view()),
    path('solregistrada/<int:usuario>/',views.SolicitudesRegistradas.as_view()),

  
    
]
