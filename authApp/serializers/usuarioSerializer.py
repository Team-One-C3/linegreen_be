from rest_framework import serializers
from authApp.models.usuario import Usuario

class UsuarioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Usuario
        fields = ['id', 'username', 'nombre', 'apellido','ciudad','password', 'barrio', 'correo', 'zona', 'telefono','direccion','tipo']

    def create(self, validated_data):
        userInstance = Usuario.objects.create(**validated_data)
        return userInstance


    def to_representation(self, obj):
        user = Usuario.objects.get(id=obj.id) 
        print (self)
        print(id, obj.id)
        return {
                    'id_usuario': user.id,
                    'username': user.username,
                    'nombre': user.nombre,
                    'apellido':user.apellido,
                    'direccion':user.direccion,
                    'ciudad': user.ciudad,
                    'correo': user.correo,
                    'barrio':user.barrio,
                    'zona': user.zona,
                    'telefono': user.telefono,
                    'password':user.password,
                    'tipo':user.tipo,
        }
