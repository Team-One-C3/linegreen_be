from authApp.models.tips import Tips
from authApp.models.usuario import Usuario
from rest_framework import serializers

class TipSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tips
        fields = ['id_tip', 'descripcion', 'puntuacion']
        
    def create(self,validated_data):
           userInstance = Tips.objects.create(**validated_data)
           return userInstance
    
    def to_representation(self,obj):
        aviso = Tips.objects.get(id=obj.id)
        user = Usuario.objects.get(id=obj.id)
        return{
            'id_tip' : aviso.id_tip,
            'descripcion' : aviso.descripcion,
            'puntuacion' : aviso.puntuacion,
        }