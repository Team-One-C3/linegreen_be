
from rest_framework import serializers
from authApp.models.recolector import Recolector

class RecolectorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Recolector
        fields = ['id_usuario', 'username', 'nombre', 'apellido','ciudad','password', 'barrio', 'correo', 'zona', 'telefono']

    def create(self, validated_data):
        userInstance = Recolector.objects.create(**validated_data)
        return userInstance

    def to_representation(self, obj):
        user = Recolector.objects.get(id=obj.id)
        return {
                    'id_usuario': user.id_usuario,
                    'username': user.username,
                    'nombre': user.nombre,
                    'apellido':user.apellido,
                    'direccion':user.direccion,
                    'ciudad': user.ciudad,
                    'correo': user.correo,
                    'barrio':user.barrio,
                    'zona': user.zona,
                    'telefono': user.telefono,
                    'password':user.password,
                    'puntaje':user.puntaje,
                    'tipo_recolector':user.tipo_recolector
        }
