from authApp.models import DetalleSolicitud
from authApp.models.usuario import Usuario
from rest_framework import serializers

class DetalleSerializer (serializers.ModelSerializer):
    class Meta:
        model = DetalleSolicitud
        fields = ['id_detalle','cantidad', 'unidad_medida', 'observaciones']

    def create(self,validated_data):
       userInstance = DetalleSolicitud.objects.create(**validated_data)
       return userInstance
    
    def to_representation(self,obj):
        detalle = DetalleSolicitud.objects.get(id=obj.id)
        user = Usuario.objects.get(id=obj.id)
        return{
                'id_detalle' : detalle.id_detalle,
                'cantidad' : detalle.cantidad,
                'unidad_medida' : detalle.unidad_medida,
                'observaciones' : detalle.observaciones,
        }
