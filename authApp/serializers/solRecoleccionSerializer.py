from authApp.models.solicitudRecoleccion import SolicitudRecoleccion
from authApp.models.usuario import Usuario
from rest_framework import serializers

class SolRecoleccionSerializer (serializers.ModelSerializer):
    class Meta:
        model = SolicitudRecoleccion
        fields = ['id','fecha_solicitud','fecha_recoleccion','id_usuario','comentarios_usuarios','comentarios_recolector','puntaje_serv','recoleccion_efectiva','ubicacion_recolector','estado_sol','id_recolector']

    def create(self,validated_data):
       userInstance = SolicitudRecoleccion.objects.create(**validated_data)
       return userInstance
    
    def to_representation(self,obj):
        solRecoleccion = SolicitudRecoleccion.objects.get(id=obj.id)
        user =  Usuario.objects.get(id=solRecoleccion.id_usuario_id)
        return{
                'id' : solRecoleccion.id,
                'fecha_solicitud' : solRecoleccion.fecha_solicitud,
                'fecha_recoleccion' : solRecoleccion.fecha_recoleccion,
                'id_usuario' : user.id,
                'comentarios_usuarios' : solRecoleccion.comentarios_usuarios,
                'comentarios_recolector' : solRecoleccion.comentarios_recolector,
                'puntaje_serv' : solRecoleccion.puntaje_serv,
                'recoleccion_efectiva' : solRecoleccion.recoleccion_efectiva,
                'ubicacion_recolector' : solRecoleccion.ubicacion_recolector,
                'estado_sol':solRecoleccion.estado_sol,
                'id_recolector':user.id,
        }