from rest_framework import serializers
from authApp.models.elementos import Elementos

class ElementoSerializer(serializers.ModelSerializer): 
    class Meta:
        model = Elementos
        fields = ['id', 'nombre',  'descripcion', 'recomendaciones']
        
    def create(self, validated_data):
        userInstance = Elementos.objects.create(**validated_data)
        return userInstance
    
    def to_representation(self, obj):
        elementos = Elementos.objects.get(id=obj.id)
        
        return {
                    'id': elementos.id_elemento,
                    'nombre': elementos.nombre,
                    'descripcion': elementos.cantidad,
                    'recomendaciones': elementos.recomendaciones,
                  #  'foto': elementos.foto,
        }
from authApp.models.elementos import Elementos
from authApp.models.usuario import Usuario
from rest_framework import serializers

class ElementoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Elementos
        fields = ['id', 'nombre', 'descripcion', 'recomendaciones']
        
    def create(self,validated_data):
           userInstance = Elementos.objects.create(**validated_data)
           return userInstance
    
    def to_representation(self,obj):
        element = Elementos.objects.get(id=obj.id)

     #   user = Usuario.objects.get(id=obj.id)
        return{
            'id'                : element.id,
            'nombre'            : element.nombre,
            'descripcion'       : element.descripcion,
            'recomendaciones'   : element.recomendaciones,
        }          

