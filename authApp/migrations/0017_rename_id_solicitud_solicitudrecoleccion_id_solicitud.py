# Generated by Django 3.2.7 on 2021-10-26 13:57

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('authApp', '0016_merge_20211024_0931'),
    ]

    operations = [
        migrations.RenameField(
            model_name='solicitudrecoleccion',
            old_name='Id_solicitud',
            new_name='id_solicitud',
        ),
    ]
