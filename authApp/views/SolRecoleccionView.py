
from django.conf import settings
from django.db.models.query import QuerySet
from rest_framework import generics, status, views
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.backends import TokenBackend
from rest_framework.mixins import DestroyModelMixin

from authApp.models.solicitudRecoleccion import SolicitudRecoleccion
from authApp.serializers.solRecoleccionSerializer import SolRecoleccionSerializer

class SolRecoleccionView(views.APIView):
     serializer_class = SolRecoleccionSerializer
     permission_classes = (IsAuthenticated,)
     queryset = SolicitudRecoleccion.objects.all()

     def post(self, request, *args, **kwargs):
        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token,verify=False)

        if valid_data['user_id']!=self.kwargs['id']:
            stringResponse = {'detail': 'Unauthorized Request'}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)

        serializer = SolRecoleccionSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response({'Response':'Registro creado exitosamente'},status=status.HTTP_201_CREATED)
      

class SolicitudActualizarView(generics.UpdateAPIView):
    serializer_class = SolRecoleccionSerializer
    permission_classes = (IsAuthenticated,)
    queryset = SolicitudRecoleccion.objects.all()

    def put(self, request, *args, **kwargs):
        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token,verify=False)

        if valid_data['user_id']!=self.kwargs['id']:
            stringResponse = {'detail': 'Unauthorized Request'}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)

        super().update(request, *args, **kwargs)
        return Response({'Response':'Registro actualizado con éxito'},status=status.HTTP_200_OK)

class EliminarSolicitudView(generics.DestroyAPIView):
    serializer_class = SolRecoleccionSerializer
    permission_classes = (IsAuthenticated,)
    queryset = SolicitudRecoleccion.objects.all()

    def delete(self, request, *args, **kwargs):
        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token,verify=False)

        if valid_data['user_id']!=self.kwargs['id']:
            stringResponse = {'detail': 'Unauthorized Request'}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)
        
        super().destroy(request, *args, **kwargs)
        
        return Response({'Response':'Registro eliminado'},status=status.HTTP_200_OK)

class ConsultarSolicitudView(generics.RetrieveAPIView):
    serializer_class = SolRecoleccionSerializer
    permission_classes = (IsAuthenticated,)
    queryset = SolicitudRecoleccion.objects.all()

    def get(self, request, *args, **kwargs):
        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token,verify=False)

        if valid_data['user_id']!=self.kwargs['id']:
            stringResponse = {'detail': 'Unauthorized Request'}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)
                
        return super().get(request, *args, **kwargs)



class ConsultarPorUsuario(generics.ListAPIView):
    serializer_class = SolRecoleccionSerializer
    permission_classes = (IsAuthenticated,)
    queryset = SolicitudRecoleccion.objects.all()

    def get_queryset(self):
        token = self.request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token,verify=False)

        if valid_data['user_id']!=self.kwargs['usuario']:
            stringResponse = {'detail': 'Unauthorized Request'}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)
        queryset = SolicitudRecoleccion.objects.filter(id_usuario_id=self.kwargs['usuario'])  
        
        #queryset = SolicitudRecoleccion.objects.all()
        return queryset

class SolicitudesRegistradas(generics.ListAPIView):
    serializer_class = SolRecoleccionSerializer
    permission_classes = (IsAuthenticated,)
    queryset = SolicitudRecoleccion.objects.all()

    def get_queryset(self):
        token = self.request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token,verify=False)

        if valid_data['user_id']!=self.kwargs['usuario']:
            stringResponse = {'detail': 'Unauthorized Request'}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)
        queryset = SolicitudRecoleccion.objects.filter(estado_sol="registrada")  
        
        
        return queryset
