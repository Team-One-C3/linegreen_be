
from django.conf import settings
from django.db.models.query import QuerySet
from rest_framework import generics, status
from rest_framework import response
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.backends import TokenBackend

from authApp.models.elementos import Elementos
from authApp.serializers.elementoSerializer import ElementoSerializer

class ElementosDetalle(generics.ListAPIView):
    #queryset = Elementos.objects.all()
    serializer_class = ElementoSerializer
    permission_classes= (IsAuthenticated,)

    def get_queryset(self):
        print("Request :", self.request)
        print("Args :", self.args)
        print("Kwargs :",self.kwargs)

        token = self.request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token,verify=False)

        if valid_data['user_id']!=self.kwargs['usuario']:
            stringResponse = {'detail': 'Unauthorized Request'}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)
        
       
        queryset = Elementos.objects.filter(id=self.kwargs['elemento'])
        return queryset
       # return super().get(request, *args, **kwargs)

class ElementoActualizar(generics.UpdateAPIView):
    serializer_class = ElementoSerializer
    permission_classes = (IsAuthenticated,)
    queryset = Elementos.objects.all()

    def put(self, request, *args, **kwargs):
        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token,verify=False)

        if valid_data['user_id']!=self.kwargs['usuario']:
            stringResponse = {'detail': 'Unauthorized Request'}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)
        
        return super().update(request, *args, **kwargs)
        
        

       
class ElementoEliminar (generics.DestroyAPIView):
    serializer_class = ElementoSerializer
    permission_classes = (IsAuthenticated,)
    queryset = Elementos.objects.all()

    def delete(self, request, *args, **kwargs):
        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token,verify=False)

        if valid_data['user_id']!=self.kwargs['usuario']:
            stringResponse = {'detail': 'Unauthorized Request'}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)
        return super().destroy(request, *args, **kwargs)
