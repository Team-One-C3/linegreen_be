from rest_framework import status, views
from rest_framework.response import Response
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

from authApp.serializers.tipSerializer import TipSerializer

class TipCreateView(views.APIView):   
    def post(self, request, *args, **kwargs):
        serializer = TipSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save() 
        
        return Response(status=status.HTTP_201_CREATED)
    
    def get(self, request, *args, **kwargs):
        return Response(status=status.HTTP_200_OK)
    
    def put(self, request, *args, **kwargs): 
       return Response(status=status.HTTP_201_CREATED)
   
    def delete(self, request, *args, **kwargs): 
       serializer = TipSerializer(data=request.data)
       serializer.is_valid(raise_exception=True)
       serializer.save()  
       return Response(status=status.HTTP_200_OK)
    