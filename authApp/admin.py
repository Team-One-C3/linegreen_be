from django.contrib import admin
from .models.usuario import Usuario
from .models.solicitudRecoleccion import SolicitudRecoleccion
from .models.elementos import Elementos
from .models.detalleSolicitud import DetalleSolicitud
from .models.tips import Tips
from .models.recolector import Recolector

# Register your models here.
admin.site.register(Usuario)
admin.site.register(SolicitudRecoleccion)
admin.site.register(Elementos)
admin.site.register(DetalleSolicitud)
admin.site.register(Tips)
admin.site.register(Recolector)

