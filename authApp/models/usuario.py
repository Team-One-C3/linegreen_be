from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.contrib.auth.hashers import make_password

class UserManager(BaseUserManager):
    def create_user(self, username, password=None):
        """
        Creates and saves a user with the given username and password.
        """
        if not username:
            raise ValueError('Users must have an username')
        user = self.model(username=username)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, password):
        """
        Creates and saves a superuser with the given username and password.
        """
        user = self.create_user(
            username=username,
            password=password,
        )
        user.is_admin = True
        user.save(using=self._db)
        return user

class Usuario(AbstractBaseUser, PermissionsMixin):
    id = models.BigAutoField(primary_key=True)
    username = models.CharField('Username',max_length=20, unique=True)
    nombre = models.CharField('Nombre', max_length = 45, unique=True)
    apellido = models.CharField('Apellido',max_length=46)
    direccion = models.CharField('Direccion', max_length = 47)
    ciudad = models.CharField('Ciudad',max_length=48)
    correo = models.EmailField('Correo', max_length = 49)
    barrio = models.CharField('Barrio', max_length = 50)
    zona = models.CharField('Zona', max_length = 51)
    telefono = models.CharField('Telefono', max_length = 52)
    password = models.CharField('Password', max_length = 256)
    tipo=models.CharField('tipo',max_length=50, default = "None")
    
    def save(self, **kwargs):
        some_salt = 'mMUj0DrIK6vgtdIYepkIxN'
        self.password = make_password(self.password, some_salt)
        super().save(**kwargs)
    
    objects = UserManager()
    USERNAME_FIELD = 'username'