from django.db import models

class Recolector(models.Model):
    id_usuario = models.BigAutoField(primary_key=True)
    username = models.CharField('Username',max_length=20, unique=True)
    nombre = models.CharField('Nombre', max_length = 50, unique=True)
    apellido = models.CharField('Apellido',max_length=50)
    direccion = models.CharField('Direccion', max_length = 100)
    ciudad = models.CharField('Ciudad',max_length=50)
    correo = models.EmailField('Correo', max_length = 50)
    barrio = models.CharField('Barrio', max_length = 50)
    zona = models.CharField('Zona', max_length = 51)
    telefono = models.CharField('Telefono', max_length = 20)
    password = models.CharField('Password', max_length = 256)
    puntaje =models.BigIntegerField ('Puntaje')
    tipo_recolector= models.CharField ('Tipo_recolector',max_length= 50)