from django.db import models
from django.db.models.deletion import SET_DEFAULT
from .usuario import Usuario

class SolicitudRecoleccion(models.Model):
    id = models.AutoField(primary_key=True)
    fecha_solicitud = models.DateField()
    fecha_recoleccion = models.DateField()
    id_usuario = models.ForeignKey(Usuario, related_name="usuario", on_delete=models.CASCADE)
    comentarios_usuarios = models.CharField('Comentarios_Usuario', max_length = 250)
    comentarios_recolector = models.CharField('Comentarios_Recolector', max_length = 250)
    puntaje_serv = models.IntegerField(default=0)
    recoleccion_efectiva = models.BooleanField(default=False)
    ubicacion_recolector = models.CharField('Ubicacion_Recolector', max_length = 250)
    estado_sol = models.CharField('Estado_Solicitud',max_length=250)
    id_recolector = models.ForeignKey(Usuario, related_name="recolector", on_delete=models.CASCADE)