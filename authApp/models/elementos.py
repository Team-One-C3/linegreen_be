from django.db import models
from .usuario import Usuario
 
class Elementos (models.Model):
    id = models.BigAutoField(primary_key=True)
    nombre = models.CharField('Nombre', max_length = 15)
    descripcion = models.CharField('descripcion', max_length = 250)
    recomendaciones = models.CharField('recomendaciones', max_length = 250)
    