from django.db import models
from .usuario import Usuario

 
class DetalleSolicitud (models.Model):
    id_detalle = models.BigAutoField(primary_key=True)
    cantidad = models.IntegerField('cantidad')
    unidad_medida = models.CharField('unidad_medida', max_length = 20)
    observaciones = models.CharField('observaciones', max_length = 250)
    