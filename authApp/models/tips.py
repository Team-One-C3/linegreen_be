from django.db import models
from .usuario import Usuario

 
class Tips (models.Model):
    id_tip = models.BigAutoField(primary_key=True)
    descripcion = models.CharField('descripcion', max_length = 250)
    puntuacion = models.IntegerField('puntuacion')
    