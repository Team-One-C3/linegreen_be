from .usuario import Usuario
from .solicitudRecoleccion import SolicitudRecoleccion
from .elementos import Elementos
from .detalleSolicitud import DetalleSolicitud
from .tips import Tips
from .recolector import Recolector
